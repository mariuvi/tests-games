-- // create questions table
-- Migration SQL that makes the change goes here.
CREATE TABLE IF NOT EXISTS t_questions (
`id` INT unsigned AUTO_INCREMENT PRIMARY KEY,
`quiz_id` INT unsigned null,
`question` TEXT NOT NULL ,
FOREIGN KEY `questions_quiz_id` (`quiz_id`)
                                       REFERENCES `t_quizes` (`id`)
                                       on UPDATE cascade
                                       on DELETE set null
);
-- @UNDO
-- SQL to undo the change goes here.
drop table  if exists t_questions;