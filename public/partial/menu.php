<?php
declare(strict_types=1);
use \AnyTests\Models\Category;
$categoryModel = New Category();
$categories = $categoryModel->get();
?>

<header>
        <ul>
            <li style="display: inline; margin: 20px">
                <a href="/">Home </a>
            </li>
            <?php
            foreach ($categories as $category){
                ?>
                <li style="display: inline; margin: 20px">
                    <a href="/?category=<?= $category['slug'] ?>"><?= $category['title']?> </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </header>
