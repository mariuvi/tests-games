-- // create categories table
-- Migration SQL that makes the change goes here.
CREATE TABLE IF NOT EXISTS t_categories (
id integer unsigned AUTO_INCREMENT primary key ,
title varchar(191) UNIQUE ,
slug varchar(191) unique
                                        )
-- @UNDO
-- SQL to undo the change goes here.
DROP TABLE IF EXISTS t_categories;
