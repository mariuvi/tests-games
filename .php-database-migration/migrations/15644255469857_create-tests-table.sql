-- // create tests table
-- Migration SQL that makes the change goes here.
CREATE table if not exists t_quizes (
`id` INT unsigned auto_increment primary key,
`quiz` varchar (191) NOT NULL,
`slug` varchar (191) not null unique
);
-- @UNDO
-- SQL to undo the change goes here.
DROP TABLE if EXISTS t_quizes;
